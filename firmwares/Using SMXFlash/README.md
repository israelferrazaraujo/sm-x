# Firmwares update for SM-X

<B>Don´t use this firmwares on Zemmix or OCM it´s for SM-X only!</B>
<BR>


Oduvaldo Pavan Junior video tutorial, english audio with portuguese subtitles.

[![ScreenShot](http://network.bbtv.com/pt/wp-content/uploads/sites/2/2016/02/Postar-V%C3%ADdeo-no-Youtube.png)](https://www.youtube.com/watch?v=HNom3F6vyYE)


The "BR", "US", "FR" and "SP" extensions are reffering the different keyboard maps (brazilian, USA, french and spanish layouts). Choose just ONE file to update, according to your keyboard.


Update example, after booting your SM-X into MSX-DOS, type: 

<span style="font-family: Courier">SMXFLASH SM-X_BR.PLD</span>

<b>ATTENTION! Wait for both counters to finish!</b> 

It´s takes about one minute from the start to the end.

Then just turn off the SM-X and turn on again. It´s updated!

Special thanks to Luca Chiodi to provide the new SMXFLASH.COM for the SM-X.

