# Firmwares for SM-X

<B>ATTENTION!!! Don´t use Zemmix or OCM firmwares on SM-X! It will brick the SM-X and you will need a USB Blaster to recover</B>
<BR>

Based on lastest version of Luca Chiodi (KdL) port, currently at <b>3.7.1</b>

You can download his original source in

https://gnogni.altervista.org/



Changelog, specific to SX-X firmware


2019-10-03 ver 1.3 by Victor Trucco
- PS/2 Mouse support. (Emulated in joystick Port A)
- OPLL fix by MiSTer Team (thanks Oduvaldo Pavan Junior)

[![ScreenShot](http://network.bbtv.com/pt/wp-content/uploads/sites/2/2016/02/Postar-V%C3%ADdeo-no-Youtube.png)](https://www.youtube.com/watch?v=XABSd6wRcDs)

<HR>

2019-09-26 ver 1.2 by Victor Trucco
- dipswitch 7 disables the internal OPLs (shared with mapper size)
- Scanlines now selectable: button 2 toggles 25% -> 50% -> 75% -> off 

<HR>

2019-09-10 ver 1.1 by Oduvaldo Pavan Junior
- "Cadari´s bit", undocumented VDP feature discovered by Luciano Cadari on 90s.
- Blink mode added on Graphics Mode

[![ScreenShot](http://network.bbtv.com/pt/wp-content/uploads/sites/2/2016/02/Postar-V%C3%ADdeo-no-Youtube.png)](https://youtu.be/HNom3F6vyYE?t=420)

<HR>

2019-07-09 ver 1.0 by Victor Trucco
- Support to MIDI OUT on joystick port 2 (I/O ports 0xE8 and 0xE9, like internal Turbo R)

[![ScreenShot](http://network.bbtv.com/pt/wp-content/uploads/sites/2/2016/02/Postar-V%C3%ADdeo-no-Youtube.png)](https://www.youtube.com/watch?v=qK3noTjILS0)

<HR>


2019-06-30 ver 0.8 by Victor Trucco
- Support for OPL (MSX-Audio), OPL2 and OPL3

[![ScreenShot](http://network.bbtv.com/pt/wp-content/uploads/sites/2/2016/02/Postar-V%C3%ADdeo-no-Youtube.png)](https://www.youtube.com/watch?v=PJCc9vpJP_8)

<HR>


2019-05-01 ver 0.7 by Victor Trucco
- Initial HDMI support. 640x480x60hz

[![ScreenShot](http://network.bbtv.com/pt/wp-content/uploads/sites/2/2016/02/Postar-V%C3%ADdeo-no-Youtube.png)](https://youtu.be/RX7RdIxXsK0?t=580)

<HR>


2019-02-12 ver 0.6 by Oduvaldo Pavan Junior
- UART
- Initial WiFi support

[![ScreenShot](http://network.bbtv.com/pt/wp-content/uploads/sites/2/2016/02/Postar-V%C3%ADdeo-no-Youtube.png)](https://youtu.be/F1nnJxIrxyg)

<HR>


2018-07-7 ver 0.1 by Victor Trucco
- Initial firmware, based on KdL OCM/Zemmix



