# Firmwares update for SM-X using a USB Blaster cable

<B>Don´t use this firmwares on Zemmix or OCM it´s for SM-X only! Note there are different firmwares for SM-X and SM-X Mini</B>
<BR>

![USB Blaster](usbblaster.jpg)

To recover your SM-X or to update to a newer firmware a lot faster you need an USB Blaster cable and some of the Altera FPGA software installed.

<b>Option 1: Altera Stand Alone Programmer</b>

For Windows or Linux

https://www.altera.com/downloads/software/prog-software/121.html

<b>Option 2: Quartus Web edition</b>

It´s a LOT bigger, but you can build your own SM-X firmware from the source

http://dl.altera.com/13.1/?edition=web

After the install of one of the above softwares, if you are under Windows, you need to install the USB Blaster driver:

Video Tutorial by Mauro Passarinho, portuguese audio

[![ScreenShot](http://network.bbtv.com/pt/wp-content/uploads/sites/2/2016/02/Postar-V%C3%ADdeo-no-Youtube.png)](https://www.youtube.com/watch?v=wUNlaITtjcU)

## Programming the firmware

Connect the USB Blaster to the connector below the SM-X and then to a free USB port on your computer.

In Quartus you can select the menu "Tools", option "Programmer", or, the fotrth icon (with a red circle on the image below)

![Quartus](quartus1.jpg)

In the new window, if you are using the cable for the first time, you need to choose the USBBlaster port, using the button "Hardware Setup".

![Quartus](quartus2.jpg)

After you selected the cable port, you can point to the JIC file.
The "BR", "US", "FR" and "SP" extensions are reffering the different keyboard maps (brazilian, USA, french and spanish layouts). Choose just ONE file to update, according to your keyboard.
Check the boxes "Program" and "Verify" from the second row. (The Quartus will check the first row "Program").

![Quartus](quartus3.jpg)


Click on the "Start" button, then wait for the progress bar reachs the 100%. Now your SM-X is updated!

![Quartus](quartus5.jpg)