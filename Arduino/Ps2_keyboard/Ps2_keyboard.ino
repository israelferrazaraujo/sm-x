/*

 PS/2 keyboard simulator
 Victor Trucco - 19/11/2021

 Reads a key from MSX Hotbit and send as a PS/2 keystrobe

*/

#include "ps2dev.h" 

PS2dev keyboard(18,19);  // PS2dev object (18:clock, 19:data) (PC4 e PC5)

unsigned char new_keys[9][8];
unsigned char old_keys[9][8];
unsigned char row = 0;

unsigned char ps2_keys[9][8] = {
    {0x3d, 0x36, 0x2e, 0x25, 0x26, 0x1e, 0x16, 0x45}, // Y0 7 6 5 4 3 2 1 0
    {0x4c, 0x54, 0x5b, 0x5d, 0x55, 0x4e, 0x46, 0x3e}, // Y1 ; ] [ \ = - 9 8
    {0x32, 0x1c, 0x28, 0x4a, 0x49, 0x41, 0x0e, 0x52}, // Y2 B A > / . , ~ '
    {0x3b, 0x43, 0x33, 0x34, 0x2b, 0x24, 0x23, 0x21}, // Y3 J I H G F E D C 
    {0x2d, 0x15, 0x4d, 0x44, 0x31, 0x3a, 0x4b, 0x42}, // Y4 R Q P O N M L K
    {0x1a, 0x35, 0x22, 0x1d, 0x2a, 0x3c, 0x2c, 0x1b}, // Y5 Z Y X W V U T S 
    {0x04, 0x06, 0x05, 0x0b, 0x58, 0x83, 0x14, 0x12}, // Y6 F3 F2 F1 CODE CAPS GRAPH CTRL SHIFT
    {0x5a, 0xf8, 0x66, 0xe0, 0x0d, 0x76, 0x03, 0x0c}, // Y7 RET SELECT BS STOP TAB ESC F5 F4
    {0xe0, 0xe0, 0xe0, 0xe0, 0xe0, 0xe0, 0xe0, 0x29}  // Y8 RIGHT DOWN UP LEFT DEL INS HOME SPACE
};

unsigned char ps2_keys_extended[9][8] = {
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, // Y0 7 6 5 4 3 2 1 0
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, // Y1 ; ] [ \ = - 9 8
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, // Y2 B A > / . , ~ '
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, // Y3 J I H G F E D C 
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, // Y4 R Q P O N M L K
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, // Y5 Z Y X W V U T S 
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, // Y6 F3 F2 F1 CODE CAPS GRAPH CTRL SHIFT
    {0x00, 0x00, 0x00, 0x69, 0x00, 0x00, 0x00, 0x00}, // Y7 RET SELECT BS STOP TAB ESC F5 F4
    {0x74, 0x72, 0x75, 0x6b, 0x71, 0x70, 0x6c, 0x29}  // Y8 RIGHT DOWN UP LEFT DEL INS HOME SPACE
};

void init_keys (void)
{
    int i,j;  
  
    for (int i = 0; i < 9; i++) 
    {
        for (int j = 0; j < 8; j++) 
        {
            new_keys[i][j] = 1;
            old_keys[i][j] = 1;
        }
    }
}

void copy_keys (void)
{
    int i,j;  
  
    for (int i = 0; i < 9; i++) 
    {
        for (int j = 0; j < 8; j++) 
        {
            old_keys[i][j] = new_keys[i][j];
        }
    }
}

void read_rows(void)
{
    switch(row)
    {
        case 0:
                PORTB=B11111111;
                PORTC=B11111110;
                break;
                
        case 1:
                PORTB=B11111111;
                PORTC=B11111101;
                break;
                
        case 2:
                PORTB=B11111111;
                PORTC=B11111011;
                break;
                
        case 3:
                PORTB=B11111110;
                PORTC=B11111111;
                break;
                
        case 4:
                PORTB=B11111101;
                PORTC=B11111111;
                break;
                
        case 5:
                PORTB=B11111011;
                PORTC=B11111111;
                break;
                
        case 6:
                PORTB=B11110111;
                PORTC=B11111111;
                break;
                
        case 7:
                PORTB=B11101111;
                PORTC=B11111111;
                break;
                
        case 8:
                PORTB=B11011111;
                PORTC=B11111111;
                break;
    }

     delay(5); 

    new_keys[row][0] = (PIND & 0x80) ? 1:0;
    new_keys[row][1] = (PIND & 0x40) ? 1:0;
    new_keys[row][2] = (PIND & 0x20) ? 1:0;
    new_keys[row][3] = (PIND & 0x10) ? 1:0;
    new_keys[row][4] = (PIND & 0x08) ? 1:0;
    new_keys[row][5] = (PIND & 0x04) ? 1:0;
    new_keys[row][6] = (PIND & 0x02) ? 1:0;
    new_keys[row][7] = (PIND & 0x01) ? 1:0;


    //read shift key
    // It´s the tricky part, because we need to read the shift ignoring
    // the ghosting

    //set the pins as inputs with pull-ups, except the Y6
    DDRC = DDRC & B11111000; //2 downto 0 Hotbit Y2 to Y0 
    DDRB = B00001000; //5 downto 0 Hotbit Y8 to Y3
    PORTB= B11110111;
    PORTC= B11111111;
    delay(1);  // wait the pins to change
    new_keys[6][7] = (PIND & 0x01) ? 1:0; //read the shift

    //return the pins as outputs
    DDRC = DDRC | B00000111; //2 downto 0 Hotbit Y2 to Y0 
    DDRB = B11111111; //5 downto 0 Hotbit Y8 to Y3
    PORTB= B11111111;
    PORTC= B11111111;
                
  row++;

  if (row>8) 
  {
    row = 0; 
  }
}

void setup() {

  //Set port D as inputs
  DDRD = 0; // all pins as inputs 
  PORTD = B11111111;        //set all input pins pull up

  //Set port B as outputs
  DDRB = DDRB | B00111111;  //5 downto 0 as outputs - Hotbit Y8 to Y3

  //Set port C as outputs
  DDRC = DDRC | B00110111;  //2 downto 0 as outputs - Hotbit Y2 to Y0 - PC5 - Keyboard data - PC4 - Keyboard clock
  
  // Serial.begin(115200);          
  
  // Serial.println("Initialize Ok!");  
  
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);

  init_keys();
}



void loop() {

  read_rows(); // fill the entire key matrix


    //check the matrix for the keys
    // "0" is a key pressed, "1" otherwise.
   int i,j, extended;  
  
    for (int i = 0; i < 9; i++) 
    {
        for (int j = 0; j < 8; j++) 
        {
              //send the "EXTENDED" codes, if any
              if ((old_keys[i][j] == 1 && new_keys[i][j] == 0) || (old_keys[i][j] == 0 && new_keys[i][j] == 1))
              {
                  if ( ps2_keys[i][j] == 0xe0)
                  {
                      keyboard.write( ps2_keys[i][j] );
                      delay(10);
                      extended = 1;
                  }
                  else
                      extended = 0; 
              }
          
              //send the "break" codes, if any
              if (old_keys[i][j] == 0 && new_keys[i][j] == 1) 
              {
                  keyboard.write(0xf0); //send a break signal
                  delay(10); 

                  
                  if (extended)
                      keyboard.write( ps2_keys_extended[i][j] );
                  else
                      keyboard.write( ps2_keys[i][j] );
                      
                  delay(10); 
      

      
              }
      
              //send the "make" codes, if any
              if (old_keys[i][j] == 1 && new_keys[i][j] == 0) 
              {
                  if (extended)
                      keyboard.write( ps2_keys_extended[i][j] );
                  else
                      keyboard.write( ps2_keys[i][j] );
                  
                  delay(10); 
     
      
              }
        }
    }

    //copy the new_keys to old_keys  
    copy_keys();



}
